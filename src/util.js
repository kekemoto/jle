
obj = {a:'a'}

function defineProperty(obj, prop, handle){
    let value = obj[prop]
    Object.defineProperty(obj, prop, {
        get: () => handle.get(value, obj, prop),
        set: input => {
            value = handle.set(input, obj, prop)
            handle.setAfter(input, obj, prop)
            return value
        },
    })
}

defineProperty(obj, 'a', {
    get(value){return value},
    set(input){return input},
    setAfter(input){},
})
