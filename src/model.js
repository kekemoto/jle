
function toStore(store_data){
    // ネストされているObjectにも適用
    for(let prop in store_data){
        if (store_data[prop] instanceof Object){
            store_data[prop] = toStore(store_data[prop])
        }
    }

    // TODO: let にしてテスト
    var listeners = {}
    if (store_data.hasOwnProperty('addListener')){
        throw new Exception('Aleady defined "addListener"')
    }
    store_data.addListener = function(prop, fun){
        if(!listeners.hasOwnProperty(prop)){ listeners[prop] = [] }
        listeners[prop].push(fun)
    }

    return new Proxy(store_data, {
        set: function(target, prop, value, reciver){
            target[prop] = value

            // emit event
            if(!listeners.hasOwnProperty(prop)){ listeners[prop] = [] }
            listeners[prop].forEach(sub => sub.apply(target))

            return true
        }
    })
}

// レンダー関数内でストア変数が呼ばれたら、レンダー関数をストア変数のリスナーに登録
// TODO: レンダー関数を呼んでいるレンダー関数がある場合、どうするか
function toStoreForRender(render_data, store){
    console.log(store);

    // ネストされているObjectにも適用
    for(let prop in store){
        if (store[prop] instanceof Object){
            store[prop] = toStoreForRender(store[prop])
        }
    }

    return new Proxy(store, {
        get: function(target, prop, reciver){
            target[prop].addListener(prop, render_data)
            return target[prop]
        },
        set: function(target, prop, reciver){
            throw new Exception(`Store can not be changed from render. property: ${prop}, object: ${target.toString()}`)
            return false;
        }
    })
}

function toRender(render_data, store_for_render){
    for(let prop in store_for_render){
        eval(`var ${prop} = ${store_for_render[prop]}`)
    }
    render_data.apply(store_for_render)
    return render_data
}

// TODO: toAction()

function toModel(model_data){
    let model = {}
    model.store = toStore(model_data.store)
    model.actions = model_data.actions
    for(let prop in model_data.renders){
        render_data = model_data.renders[prop]
        model.renders[prop] = toRender(render_data, toStoreForRender(render_data, model.store))
    }
    return model
}

test = toModel({
    actions: {},
    store: {
        a: 'a',
        b: {a: 'b.a'}
    },
    renders: {
        c: function(){
            return a
        }
    },
})
