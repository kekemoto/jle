# JLE

**JSON like Excel.**

## Architecture

リアクティブ
Flux

<!-- view -> Action -> Store -> Render -> view

Action がユーザーの行動を解釈して Store の変更する
Store がデータの入れ物。データに変更があった場合に Render へ通知する
Render がデータの変更を view に反映する。

セルの値を変更をするActionを実行すると、Actionが対象のセルの Store を変更する
Store は変更されたことを検知し、変更箇所に登録されていた Render のリスナーに変更を通知する
Render が view へ反映させる

オブサーバーで書けば綺麗に書ける。
要素ごとに疎結合になってかなり綺麗になる。

関数型で宣言的に書くとどうか。
render(action(data))
まぁでもメモ化とか関数型に優しい機能は充実してないだろうし普通にオブジェクト指向で行こう

filter とか絶対挟みたくなる。その場合にオブサーバーだとコールバック地獄になるなぁ。 -->

View -> Action -> Model -> Render -> View

MVVM を一方向のデータフローにし、データ駆動で組み立てる。

Model は Action と Render を内包するオブジェクト。
Modelのデータ変更は Action によって行われ、
データ変更の通知は Render によって行われる。

Model は Model を入れ子にできる。

最終的な View とのインターフェースは
ユーザーの行動（DOM のイベント）を Model の Action に紐付け、
データの結果を Render を通し、リアクティブにDOMへ反映させる。

## Licence
[Apache License 2.0](LICENCE)
